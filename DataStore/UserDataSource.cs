﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi.DataStore
{
    public static class UserDataSource
    {
        private static AppSettings appSettings { get; set; }

        private static string GetConnectionVariablesFromConfig()
        {
            return appSettings.ConnectionString;
        }

        public static string GetUser(string username, AppSettings settings)
        {
            appSettings = settings; 
            using (var connection = new NpgsqlConnection(GetConnectionVariablesFromConfig()))
            {
                var result = "";
                connection.Open();
                List<DbParameter> parameters = new List<DbParameter>();
                var query = "SELECT array_to_json(array_agg(row_to_json(userCM))) AS readings FROM cropmetrics.user AS userCM WHERE username = @username";
                parameters.Add(new DbParameter() { Name = "@username", Value = "ab.fisher" });

                using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection))
                {
                    foreach (var param in parameters)
                    {
                        cmd.Parameters.Add(new NpgsqlParameter(param.Name.ToLowerInvariant(), param.Value));
                    }
                    result = cmd.ExecuteScalar().ToString();

                }
                connection.Close();
                return result;
            }
        }
    }
}
