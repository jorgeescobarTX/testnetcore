using System.ComponentModel.DataAnnotations;

namespace WebApi.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Token { get; set; }

        [Display(Name = "User name")]
        public string Username { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        //[Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //[Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        //[Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        //[Required]
        [Display(Name = "Full name")]
        public string FullName { get; set; }

        //[Required]
        [Display(Name = "Dealer")]
        public string Dealer { get; set; }

        //[Required]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        public string PasswordHash { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Roles { get; set; }
    }
}