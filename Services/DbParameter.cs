﻿namespace WebApi.Services
{
    public class DbParameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
}