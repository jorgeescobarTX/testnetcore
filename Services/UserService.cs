using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Identity;
using Npgsql;
using WebApi.Entities;
using WebApi.Helpers;
using Newtonsoft.Json;
using WebApi.DataStore;

namespace WebApi.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
    }

    public class UserService : IUserService
    {

        public string SignatureAlgorithm
        {
            get { return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256"; }
        }

        public string DigestAlgorithm
        {
            get { return "http://www.w3.org/2001/04/xmlenc#sha256"; }
        }


        public bool AuthenticateUser(User user, string password)
        {
            PasswordHasher<User> pwd = new PasswordHasher<User>();

            var result = pwd.VerifyHashedPassword(user, user.PasswordHash, password);

            return (result == PasswordVerificationResult.SuccessRehashNeeded || result == PasswordVerificationResult.Success);
        }

        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private List<User> _users = new List<User>
        { 
            new User { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" } 
        };

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public User Authenticate(string userName, string password)
        {
            //var user = _users.SingleOrDefault(x => x.Username == username && x.Password == password);
            var userString = UserDataSource.GetUser(userName, _appSettings);

            var user = JsonConvert.DeserializeObject<List<User>>(userString).FirstOrDefault();

            var check = this.AuthenticateUser(user, password);

            // return null if user not found
            if (check == false)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            //var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var key = Encoding.UTF8.GetBytes("53D6C1F4BF21D4C5E664AF27B384A86B2EBA2199");


            var identity = new ClaimsIdentity("Bearer");
            identity.AddClaim(new Claim(ClaimTypes.Name, user.Username));
            identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
            identity.AddClaim(new Claim(ClaimTypes.Role, "grower"));
            identity.AddClaim(new Claim("sub", user.Username));

            var signingKey = new SymmetricSecurityKey(key);
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256, DigestAlgorithm);
            //DateTime now = new DateTime(2019,1,1,1,1,1,1, DateTimeKind.Utc);
            DateTime now = DateTime.UtcNow;
            DateTime expires = now.AddMinutes(65);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = "CropMetrics",
                Audience = "all",
                IssuedAt = now,
                Subject = identity,
                Expires = expires,
                SigningCredentials = signingCredentials
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            // return users without passwords
            return _users.Select(x => {
                x.Password = null;
                return x;
            });
        }
    }
}